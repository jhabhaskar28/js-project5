function limitFunctionCallCount(callBack,noOfTimes) {
    if(callBack === undefined || typeof(callBack) !== 'function' || noOfTimes === undefined || typeof(noOfTimes)!== 'number'){
        let returnsNull = () => {
            return null;
        }
        return returnsNull;
    }
    let counter = 0;
    function invokesCallBack(){
        counter += 1;
        if(counter > noOfTimes){
            return null;
        } else {
            let callBackResult =  callBack();
            return callBackResult;
        }
    }
    return invokesCallBack;
}

module.exports = limitFunctionCallCount;