function cacheFunction(callBack) {
    if(callBack === undefined || typeof(callBack)!== 'function'){
        let returnsNull = () => {
            return null;
        }
        return returnsNull;
    }
    let cache = {};
    function invokesCallBack() {
        let functionArguments = JSON.stringify(invokesCallBack.arguments);
        if(cache[functionArguments] === undefined){
            let callBackResult = callBack();
            cache[functionArguments] = callBackResult;
            return callBackResult;

        } else {
            return cache[functionArguments];
        }
    }
    return invokesCallBack;
}

module.exports = cacheFunction;