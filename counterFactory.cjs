function counterFactory(){
    let counter = 0;
    const counterObject = {
        increment() {
            counter += 1;
            return counter;
        },
        decrement() {
            counter -= 1;
            return counter;
        }
    };
    return counterObject;
}

module.exports = counterFactory;